package com.villoga.s01activity;

public class S01Activity {

    public static void main(String[] args) {
        System.out.println("Hello, tell me who are you?");

        char name = 'J';
        int age = 17;
        double temperature = 36.7;
        boolean isSick = false;

        System.out.println("Name: " + name + "\nage: " + age + "\ntemperature: " + temperature + "\nAre you sick: " + isSick);
    }
}
